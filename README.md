My App Can Notify
=========

I had a play with the new [HTML5 notificatons](https://developer.mozilla.org/en/docs/Web/API/notification) API and I thought it was fantastic, I personally love app notifications. However I realized that you cannot use them on IE just yet (if ever?). So this lead me to think "hmm if I use the new notifications API, my app can most certainly notify, if my users use certain browsers.  Well I would like my web app to notify users irrespective of the browser they use i.e. browser independent notifications. So I figured I would write my own little script that could notify within the browser. I have also made a [website](http://cptdanko.bitbucket.org/myAppCanNotify/) that talks a little more about the libraries features and my motivation behind building it.

License
----
[BSD licence](http://opensource.org/licenses/BSD-2-Clause)

Dependencies
----
[jQuery](http://jquery.com/) Used for the slide-up animation and achieve long-polling with its ajax method
 
Getting the library
--------------
```sh
git clone https://cptdanko@bitbucket.org/cptdanko/myappcannotify.git
```
How to use the library?
--------------
```sh
<!--add dependancies to your HTML file-->
<html>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="myappcannotify.js"> </script>
<!--add a div that can contain the notifications-->
<script>
	var urlToQuery = "http://localhost:3000/test";
	var notificationsSpoken = false; // only supported by Chrome and Safari, at this stage
	//if speech is enabled, the name will be spoken with a greeting message relative to the time of the day it is
	var user = "Bhuman";
	var nPlugin = $("#notificationDiv").appNotify(urlToQuery,notificationsSpoken,user);
	function stopSpeech(){
		//if speech it enabled, it can be stopped by calling a simple method
		nPlugin.stopSpeech();
	}
</script>	
<body>
	<div id="notificationDiv"> </div>
</body>
</html>
```