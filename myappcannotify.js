(function ( $ ) {
    function NotificationPlugin(notiContainer, speak, userName){
        var notificationDiv,
            notificationHeader,
            notificationContainer = notiContainer,
            notificationContentId = "notificationContent",
            notifications = [],
            noVisibleUiNotification = false;
        this.speak = false;
        var user = userName,
            speechSupported = false;
        if(window.speechSynthesis){
            speechSupported = true;
        }
        this.noVisibleNotifications = function(){
            return noVisibleUiNotification;
        }
        this.setUser = function(name){
            user = name;
        }
        this.setContainerInvisible = function(){
            notificationContainer.style.display ="none";
        }
        this.setNotifications = function(list){
            for(var a in list){
                notifications.push(list[a]);
            }
        }
        this.stopSpeech = function(){
            if(speechSupported){
                window.speechSynthesis.cancel();
            }
        }
        this.startNotiSpeech = function(length){
            var greeting = getDayGreeting()+" "+user;
            speak(greeting);
            var message = "You have "+length+" new ",
                text =(length > 1)?"notifications":"notification";
            message+=text;
            speak(message);
        }
        this.addNotification = function(notification){
            var noti = new Notification();
            noti.text = notification.text;
            noti.title = notification.title;
            if(notification.url != undefined){
                if(notification.url.indexOf("http") <1){
                    noti.url = "http://"+notification.url;
                } else{
                    noti.url = notification.url;
                }
                

            }
            if(this.speak){
                speak(noti.text);
            }
            notifications.push(noti);
        }
        function getDayGreeting(){
            var date = new Date(),
                hours = date.getHours();
            if(hours >6 &&  hours <12){
                return "Good morning";
            } else  if(hours  >=12 && hours < 17){
                return "Good afternoon";
            } else {
                return "Good evening";
            }
        }
        function speak(textToSpeak) {
            if(speechSupported){
                var msg = new SpeechSynthesisUtterance(textToSpeak);
                msg.rate = 0.8;
                msg.volume =1;
                msg.pitch = 1
                msg.lang = "en-US"
                window.speechSynthesis.speak(msg);
            }
        }
        function Notification(){
            this.appendHtml = function(notificationContent){
                var noti = document.createElement("div"),
                    para = document.createElement("p"),
                    notiText = document.createTextNode(this.text),
                    notiLink = document.createElement("a");
                notificationHeader.style.backgroundColor = "#ddd";

                para.appendChild(notiText);
                notiLink.href = this.url;
                notiLink.target = "_blank";
                notiLink.textContent = this.title;
                notiLink.className = "title";
                $(notificationHeader).find(".title").remove();
                notificationHeader.appendChild(notiLink);
                noti.appendChild(para);
                notificationDiv.appendChild(noti);
            }
        }
        /* div: the div to send the notifications
        shouldUseSpeech: boolen to know if the notification should be spoken
        */
        this.setupNotificationDiv = function(){
            var divStyle = notificationContainer.style;
            notificationHeader = document.createElement("div");
            divStyle.backgroundColor = "#f5f5f5";
            divStyle.bottom = "0";
            divStyle.right = "0";
            divStyle.height = "80px";
            divStyle.width = "300px";
            divStyle.marginBottom = "7px";
            divStyle.marginLeft = "12px";
            divStyle.marginRight ="5px";
            divStyle.overflow = "hidden";
            divStyle.fontSize = "14px";
            divStyle.borderRadius = "4px";
            divStyle.lineHeight = "1.5";
            divStyle.border = "1px solid gray";
            divStyle.position = "fixed";
            divStyle.fontFamily = '"Helvetica Neue",Helvetica,Arial,sans-serif';
            divStyle.lineHeight = 1.5;
        }
        this.addCloseLink = function(){
            var link = document.createElement("a");
            $(link).hover(function(){
                link.style.cursor = "pointer";
            }, function(){
                link.style.cursor = "auto";
            });
            var linkStyle = link.style;
            linkStyle.fontSize = "18px";   
            linkStyle.fontWeight = "bold";   
            linkStyle.lineHeight = "18px";
            linkStyle.opacity = 0.4;
            linkStyle.color = "#000000";
            linkStyle.filter = "alpha(opacity=20)";
            link.textContent = "X";
            linkStyle.cssFloat = "right";
            var instance = this;
            link.onclick = function closeNotification(link){  
                $(notificationContainer).slideUp(400);
                setTimeout(function(){
                    
                    notificationDiv.removeChild($("#"+notificationContentId).children()[0]);
                    notifications.splice(0,1);
                    if(notifications.length == 0){
                        noVisibleUiNotification = true;
                    }
                    instance.loadNotification();
                    
                }, 500);
            }
            notificationHeader.appendChild(link);
            notificationContainer.appendChild(notificationHeader);
        }
        
        function addNotificationDiv(){
            notificationDiv = document.createElement("div");
            notificationDiv.id = notificationContentId;
            notificationContainer.appendChild(notificationDiv);
        }
        this.loadNotification = function(){
            notificationLength = $(notificationContainer).children().length;
            if(notificationLength < 2){
                addNotificationDiv();
            }
            if(notifications[0]){
                notifications[0].appendHtml(notificationDiv);
                $(notificationContainer).slideDown(300);
                noVisibleUiNotification = false;
            } 
        }
    }
    function addIncomingNoti(nPlugin,recevingN){
        if(recevingN.length > 0){
            if(recevingN.length >0 && nPlugin.speak){
                nPlugin.startNotiSpeech(recevingN.length);
            }
            for(var i=0; i< recevingN.length; i++){
                var notification = recevingN[i];
                nPlugin.addNotification(notification);
            }   
            if(nPlugin.noVisibleNotifications()){
                nPlugin.loadNotification();
            }
        }
    }
    $.fn.appNotify = function(url, speak, userName){
        var nPlugin = new NotificationPlugin(this[0]);
        nPlugin.setContainerInvisible();
        nPlugin.speak = speak;
        nPlugin.setUser(userName);
        nPlugin.setupNotificationDiv();
        nPlugin.addCloseLink();
        nPlugin.loadNotification();

        (function poll(){
        $.ajax({ url: url, success: function(data){
                console.log(data);
                addIncomingNoti(nPlugin,data);
            }, dataType: "json", complete: poll});
        })();
        return nPlugin;
    };
}(jQuery));