var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/test', function(req, res){
	res.setHeader('Access-Control-Allow-Origin','*');
	setTimeout(function(){
		var nList = [];
        nList.push({"text":"Test report a","title":"Test Rpt","url":"www.msn.com"});
        nList.push({"text":"Modified report b","title":"Check","url":"www.yahoo.com"});
        nList.push({"text":"Enhanced report c","title":"Review","url":"www.google.com"});
		res.send(JSON.stringify(nList));
	},60000);
  	
});

app.get('/myappcannotify.js', function(req, res){
  res.setHeader('Access-Control-Allow-Origin','*');
  res.sendfile("myappcannotify.js");
});

app.get('/', function(req, res){
  res.setHeader('Access-Control-Allow-Origin','*');
  res.sendfile('notifications.html');
});

app.get('/notifications', function(req, res){
  res.setHeader('Access-Control-Allow-Origin','*');
  res.sendfile('notifications.html');
});
http.listen(3000, function(){
  console.log('listening on *:3000');
});