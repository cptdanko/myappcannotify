Test kit for myappcannotify plugin
=========

This is a simple test server that demonstrates how the notification plugin works in the browser. 

Dependencies
----
In addition to the dependencies for the plugin, this test-kit also requires [node.js](http://nodejs.org/),[express](http://expressjs.com/) and [socket.io](http://socket.io/). 

Installing dependancies
--------------
First install node.js by downloading it from [nodejs.org](http://nodejs.org/download/).

One node.js is installed, you should be able to run npm from the command line. Installing the other dependencies is much simpler, as they dependencies are defined in the package.json file. So simply run the following commands,

```sh
cd tetKit
npm install
```
That should install everything needed to run the test server

Running the tests
--------------
From the testKit directory simply run, or if you are in the directory which 
```sh
node index.js
```
or if you are in the parent directory of testKit, try

```sh
node testKit/index.js
```
Once you have the server running simply, type the following url http://localhost:3000/ into the browser and look at the notifications on the bottom-right corner of your web-page. The server is configured to send mock notifications every 9 seconds, before you receive notifications from the server the plugin pre-loads the web-page with 2 notifications. 